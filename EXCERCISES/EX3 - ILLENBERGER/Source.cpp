#include <iostream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

struct Item {
	string name;
	int gold;
};

void generateLootedItem(Item* loot) {
	string availableDrops[5] = { "Mithril Ore", "Sharp Talon", "Thick Leather", "Jellopy", "Cursed Stone" };
	int dropValue[5] = { 100, 50, 25, 5, 0 };
	int generateIndex = rand() % 5;

	//assign values here.
	loot->name = availableDrops[generateIndex];
	loot->gold = dropValue[generateIndex];
}

//struct function as alternative but i dont quite understand it yet so ill just comment it out lol
/*Item* generateLootedItem() {
	string availableDrops[5] = { "Mithril Ore", "Sharp Talon", "Thick Leather", "Jellopy", "Cursed Stone" };
	int dropValue[5] = { 100, 50, 25, 5, 0 };
	int generateIndex = rand() % 5;

	Item* loot = new Item;

	//assign values here.
	loot->name = availableDrops[generateIndex];
	loot->gold = dropValue[generateIndex];

	//returning structure here
	return loot;
}*/

void enterDungeon(int &playerCurrentGold, string &playerAnswer); //forward declaration for neatness

int main() {

	srand(time(0));

	string playerAnswer;

	int playerCurrentGold = 50;

	while (playerCurrentGold > 0) {
		cout << "You currently have " << playerCurrentGold << "G on you.\n\n";

		enterDungeon(playerCurrentGold, playerAnswer); //starts the looting phase and new dungeon instance

		if (playerCurrentGold >= 500) {
			//checks if player has reached target goal and kicks them out of dungeon loop cuz they won already. 
			break;;
		}
	}

	if (playerCurrentGold >= 500) {
		cout << "Congratulations. You managed to reach the 500G goal.\n";
		if (playerCurrentGold > 500) {
			cout << "Wow, you managed to surpass it even! Amazing work for earning " << playerCurrentGold << "G.";
		}
	}
	else if (playerCurrentGold <= 0) {
		cout << "Guess you're just that unlucky to lose all of your gold. Sorry better luck next time, adventurer.\n";
		cout << "It's time for you to get an actual job now to earn back the money you lost.\n\n";
	}
	return 0;
}

void enterDungeon(int &playerCurrentGold, string &playerAnswer) {
	int acquiredDungeonGold = 0;
	bool inDungeon = 1;

	Item* loot = new Item;

	//statement informs player their current financial status and cost of entering a dungeon.
	cout << "Entering a dungeon costs 25G.\n" <<
		"Would you like to head into the dungeon to loot for items? (Type 'yes' or 'no')\n"; //ask player if they wanna enter dungeon
	cin >> playerAnswer;

	if (playerAnswer == "yes") {
		playerCurrentGold -= 25;
		cout << "\n25G was deducted from your gold. You now currently have " << playerCurrentGold << "G.\n\n";

		inDungeon = 1; //sets it back to 1 when player says yes when going in deeper
		int multiplier = 1;
		while (inDungeon == 1) {

			cout << "You go further into the dungeon and ventured forth with baited breath.\n\n" <<
				"You encountered a few monsters and succesfully fought them off.\nThey seem to have dropped something...\n" <<
				"...\n...\n...\n";
			cout << "You got a ";

			//call generate loot item function here
			generateLootedItem(loot);

			cout << loot->name << " whose value is about " << loot->gold << "G.\n";
			
			if (loot->name != "Cursed Stone") {
				loot->gold = loot->gold * multiplier;
				cout << "Item multiplier increases by " << multiplier << ". It's value is now " << loot->gold << "G. \n\n";

				acquiredDungeonGold += loot->gold; //adds the new value of each looted item w/ the multiplier. 

				cout << "Total gold you've acquired in this instance so far is " << acquiredDungeonGold << "G.\n";

				system("pause");
				system("cls");

				//check if they wanna head deeper here
				cout << "Great work. Seems like there's more to explore here though...would you like to venture in deeper, adventurer?\n";
				cout << "(Type 'yes' or 'no')\n";
				cin >> playerAnswer;

				if (playerAnswer == "yes") {
					multiplier++;
					cout << endl;
				}
				else {
					cout << "\nYou decided to call it a day and exit the dungeon.\n" <<
						"'It's good to not get too greedy.' you thought.\n\n";

					cout << "===================================================" << endl;
					cout << "DUNGEON GOLD ACQUIRED: " << acquiredDungeonGold << "G" << endl;
					cout << "===================================================" << endl;

					system("pause");
					system("cls");

					playerCurrentGold += acquiredDungeonGold;

					cout << "===================================================" << endl;
					cout << "TOTAL CURRENT GOLD: " << playerCurrentGold << "G" << endl;
					cout << "===================================================" << endl;

					cout << "You pay a visit back to the dungeon the following day.\n\n";
					inDungeon = 0;
				}
			}
			else {
				cout << "Uh oh..this isn't good. Get out of here quick!\n" <<
					"The Cursed Stone sucks in all the items you looted in the dungeon \n" <<
					"before you could even react and throws you teleports you out of here.\n\n";
				cout << "Well that was a frustrating...\n";

				cout << "You decide to come back another day and cool off for now.\n\n";

				system("pause");
				system("cls");

				inDungeon = 0;
			}
		}
	}
	else {
		cout << "Okay, see you around then.";
		system("pause");
		system("cls");
	}
}