#include "Heal.h"

Heal::Heal(string name, Unit* caster)
	: Skill(name, caster)
{
	 mHPRecovered = 10;
}

void Heal::activate(Unit* target)
{
	target->setHP(target->getHP() + mHPRecovered); //update health to +10
	cout << target->getName() << " is healed by " << mHPRecovered << " points!\n\n";
}
