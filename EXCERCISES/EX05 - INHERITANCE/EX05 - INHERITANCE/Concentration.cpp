#include "Concentration.h"

Concentration::Concentration(string name, Unit* caster)
	: Skill(name, caster)
{
	mDexterityUp = 2; 
}

void Concentration::activate(Unit* target)
{
	target->setDEX(target->getDEX() + mDexterityUp); //update dexterity to +2 
	cout << target->getName() << " dexterity is boosted by " << mDexterityUp << " points!\n\n";
}
