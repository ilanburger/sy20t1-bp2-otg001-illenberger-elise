#pragma once
#include <string>
#include <iostream>
#include "Unit.h"
using namespace std;

class Skill 
{
	public:
		Skill(string name, Unit* caster);
		
		string getName(); 
		Unit* getCaster(); 

		//virtual method make it polymorphic and will basically take on the argument of whatever function overrides it ONCE u put CONTEXT.
		virtual void activate(Unit* target) = 0; //set to 0 so it's defined as abstract

private: 
	string mName; 
	Unit* mCaster; 
};

