#pragma once
#include <string>
#include <iostream>
#include "Skill.h"
using namespace std;

class Might : public Skill
{
public:
	//constructor
	Might(string name, Unit* caster);
	//methods
	void activate(Unit* target) override; //power +2 

private:
	int mPowerUp;

};

