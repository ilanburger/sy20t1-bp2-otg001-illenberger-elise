#pragma once
#include <string>
#include <iostream>
#include "Skill.h"
using namespace std;

class Haste : public Skill
{
public:
	//constructor
	Haste(string name, Unit* caster);
	//methods
	void activate(Unit* target) override; //agility +2

private:
	int mAgilityUp = 10;

};

