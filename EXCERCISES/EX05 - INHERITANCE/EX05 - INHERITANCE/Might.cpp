#include "Might.h"

Might::Might(string name, Unit* caster)
	: Skill(name, caster)
{
	mPowerUp = 2; 
}

void Might::activate(Unit* target)
{
	target->setPOW(target->getPOW() + mPowerUp); //update power to +2 
	cout << target->getName() << " power is boosted by " << mPowerUp << " points!\n\n";
}
