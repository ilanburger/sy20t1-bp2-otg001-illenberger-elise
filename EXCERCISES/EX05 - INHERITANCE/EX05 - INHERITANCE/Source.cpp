#include <iostream>
#include <string>
#include <time.h>
#include "Unit.h"
#include "Skill.h"
#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Concentration.h"
#include "Haste.h"

using namespace std;

void printUnitStats(Unit* player) {
	cout << "=========================================================\n";
	cout << "UNIT:\n\n";
	cout << "Name: " << player->getName() << endl;
	cout << "HP: " << player->getHP() << endl;
	cout << "POW: " << player->getPOW() << endl;
	cout << "VIT: " << player->getVIT() << endl;
	cout << "AGI: " << player->getAGI() << endl;
	cout << "DEX: " << player->getDEX() << endl;
	cout << "=========================================================\n\n";
}

void createSkills(Unit* player) {
	//instantiate the skill types (derived classes) first. (already inherited the base class)
	//derived classes have access to both their exclusive methods and parent's methods
	Heal* skill1 = new Heal("Heal", player);
	Might* skill2 = new Might("Bonk", player);
	IronSkin* skill3 = new IronSkin("Blockt", player);
	Concentration* skill4 = new Concentration("Caffeine", player);
	Haste* skill5 = new Haste("Nyoom", player);
	
	// or u can implicit upcast | typecast = change the mask so u can put it into vector 
	//casting it back to parent restricts its access to its exlusive derived method
	/*		Skill* skill1 = new Heal("Heal");
			Skill* skill2 = new Might("Bonk");
			Skill* skill3 = new IronSkin("Blockt");
			Skill* skill4 = new Concentration("Caffeine");
			Skill* skill5 = new Haste("Nyoom");*/

	//this pushes the newly created skills into the empty pocket of skillroster the unit has
	//pushing to base class vector changes its derived's data type into that of base automatically?
	player->addSkill(skill1);
	player->addSkill(skill2);
	player->addSkill(skill3);
	player->addSkill(skill4);
	player->addSkill(skill5);
}

void pickSkill(Unit* player) {
	int index;
	index = rand() % 5; //randomly determine which skill is called

	cout << player->getName() << " used " << player->getSkills()[index]->getName() << ".\n";
	player->getSkills()[index]->activate(player);
}

int main() {
	srand(time(0));
	string playerName; 

	cout << "=========================================================\n";
	cout << "Welcome to adventurer! \n" << "What is your name, dear traveller?\n";
	cin >> playerName;
	Unit* player = new Unit(playerName, 100, 87, 67, 55, 89); //instantiate a unit

	cout << "Nice to meet you, " << player->getName() << ".\n"; 
	
	printUnitStats(player);

	cout << "We will now gift you a multitude of skills for your own benefit. These skills can increase your stats or heal your character up.\n";

	createSkills(player);

	cout << "...\n...\n...\n\nTry checking your skill roster now and see what kind of skills you have available!\n";

	player->printSkillList(); 

	cout << "\n\nNow we'll demonstrate what each skill does to your character!\n";

	while (1) { 
		pickSkill(player); 
		printUnitStats(player);

		cout << "Check the next one!\n";
		system("pause");
		system("cls");
	}

}