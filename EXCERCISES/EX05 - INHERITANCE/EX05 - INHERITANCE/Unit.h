#pragma once
#include <string>
#include <iostream>
#include <vector>
using namespace std;

class Skill; 

class Unit
{
	//unit is a base class
	public:
		//skill
		Unit(string name, int hp, int pow, int vit, int dex, int agi);

		//getters & setters
		string getName();
		int getHP();
		void setHP(int value);

		int getPOW();
		void setPOW(int value);

		int getVIT();
		void setVIT(int value);

		int getDEX();
		void setDEX(int value);

		int getAGI();
		void setAGI(int value);

		//getter for complex?
		vector<Skill*>& getSkills();
		
		//methods
		void printSkillList(); //Get a list of all items. & lets u manipulate it freely across the code 
		void addSkill(Skill* skill); //Add skills to roster.


	private: 
		string mName; 
		int mHP,
			mPOW,
			mVIT,
			mDEX,
			mAGI;

		vector<Skill*> mSkills;


};

