#pragma once
#include <string>
#include <iostream>
#include "Skill.h"
using namespace std;

class IronSkin : public Skill
{
public:
	//constructor
	IronSkin(string name, Unit* caster);
	//methods
	void activate(Unit* target) override; //vitality +2

private:
	int mVitalityUp = 2;

};

