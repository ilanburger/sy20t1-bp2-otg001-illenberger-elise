#include "IronSkin.h"

IronSkin::IronSkin(string name, Unit* caster)
	: Skill(name, caster)
{
	mVitalityUp = 2;
}

void IronSkin::activate(Unit* target)
{
	target->setVIT(target->getVIT() + mVitalityUp); //update vitality to +2 
	cout << target->getName() << " vitality is boosted by " << mVitalityUp << " points!\n\n";
}
