#include "Haste.h"

Haste::Haste(string name, Unit* caster)
	: Skill(name, caster)
{
	mAgilityUp = 2;
}

void Haste::activate(Unit* target)
{
	target->setAGI(target->getAGI() + mAgilityUp); //update agility to +2 
	cout << target->getName() << " agility is boosted by " << mAgilityUp << " points!\n\n";
}
