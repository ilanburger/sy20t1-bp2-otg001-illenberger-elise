#pragma once
#include <string>
#include <iostream>
#include "Skill.h"
using namespace std;

class Concentration : public Skill
{
public:
	//constructor
	Concentration(string name, Unit* caster);
	//methods
	void activate(Unit* target) override; //dex+ 2 

private:
	int mDexterityUp = 2; 

};

