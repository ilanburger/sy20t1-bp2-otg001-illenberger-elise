#pragma once
#include <string>
#include <iostream>
#include "Skill.h"
using namespace std;

class Heal : public Skill
{
public: 
	//constructor
	Heal(string name, Unit* caster);
	//methods
	void activate(Unit* target) override; //heal hp by 10pts

private:
	int mHPRecovered;
};

