#include "Skill.h"


Skill::Skill(string name, Unit* caster)
{
    mName = name;
    mCaster = caster;
}

string Skill::getName()
{
    return mName; 
}

Unit* Skill::getCaster()
{
    return mCaster;
}
