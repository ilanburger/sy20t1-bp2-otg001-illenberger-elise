#include "Unit.h"
#include "Skill.h"

Unit::Unit(string name, int hp, int pow, int vit, int dex, int agi)
{
    mName = name; 
    mHP = hp;
    mPOW = pow;
    mVIT = vit;
    mDEX = dex; 
    mAGI = agi;

}

string Unit::getName()
{
    return mName;
}

int Unit::getHP()
{
    return mHP;
}

int Unit::getPOW()
{
    return mPOW;
}

int Unit::getVIT()
{
    return mVIT;
}

int Unit::getDEX()
{
    return mDEX;
}

int Unit::getAGI()
{
    return mAGI;
}

void Unit::setHP(int value)
{
    mHP = value;
}

void Unit::setPOW(int value)
{
    mPOW = value;

}

void Unit::setVIT(int value)
{
    mVIT = value;

}

void Unit::setDEX(int value)
{
    mDEX = value;

}

void Unit::setAGI(int value)
{
    mAGI = value;
}

vector<Skill*>& Unit::getSkills()
{
    return mSkills; 
}

void Unit::printSkillList()
{
    for (int i = 0; i < mSkills.size(); i++) {
        cout << "["<< i+1 << "] " << mSkills[i]->getName() << "\n"; 
    }

    cout << endl << endl; 
}

void Unit::addSkill(Skill* skill)
{
    //skill is the new skill u made being added into the skill roster ur unit owns.
    
     mSkills.push_back(skill);

}
