#include <iostream>
#include <string.h>
#include <time.h>

using namespace std; 

int diceRolls(int& dice) {
	return dice = rand() % 6 + 1;
}

int betGoldDeduction(int betValue, int& gold) { 
	return gold -= betValue;
}

int payoutWin(bool snakeEyesCheck, int& payoutValue, int& gold) {
	 if (snakeEyesCheck == 1) {
			payoutValue *= 3;
			return gold += payoutValue;
	}
	else {
		payoutValue *= 2;
		return gold += payoutValue;
	}
}

int payoutDraw(int& payoutValue, int& gold) {
	payoutValue *= 1; 
	return gold += payoutValue;
}

void bet(int& currentPlayerGold) {
	int dice = 0,
		playerBet,
		botRoll1, botRoll2, botRollValue,
		playerRoll1, playerRoll2, playerRollValue;
	bool snakeEyes = 0,
		enemySnakeEyes = 0,
		draw = 0;
	
	cout << "\nHow much would you like to gamble?\n";
	cin >> playerBet;

	if (playerBet == 0 || playerBet > currentPlayerGold) {
		cout << "Invalid bet. Please try a different value that isn't 0 or higher than the current gold you have.\n\n";
	}
	else {
		betGoldDeduction(playerBet, currentPlayerGold);

		cout << "\n\nYou have betted " << playerBet << "G and will be deducted from your gold. Your current money now is " << currentPlayerGold <<
			"G. \nIf your roll is higher than the bots, you win your bet.\n\nAI is rolling dice now...";

		diceRolls(botRoll1),
		diceRolls(botRoll2);
		botRollValue = botRoll1 + botRoll2;

		cout << "\nThe AI's rolls are " << botRoll1 << " and " << botRoll2 << endl;
		cout << "\nPlayer is rolling now...";

		diceRolls(playerRoll1),
		diceRolls(playerRoll2);
		playerRollValue = playerRoll1 + playerRoll2;

		//SNAKE EYE CHECK
		if (playerRoll1 == 1 && playerRoll2 == 1) {
			snakeEyes = 1;
		}
		if (botRoll1 == 1 && botRoll2 == 1) {
			enemySnakeEyes = 1;
		}

		cout << "\nThe player's rolls are " << playerRoll1 << " and " << playerRoll2 << endl << endl;

		//DETERMINING IF WIN OR LOSE
		 if ((playerRollValue == botRollValue) || (snakeEyes == 1 && enemySnakeEyes == 1)) {
			cout << "It's a draw. You don't earn anything but you retain your money earlier.";

			payoutDraw(playerBet, currentPlayerGold);
			snakeEyes = 0;
			enemySnakeEyes = 0; 
			draw = 0;

			cout << "Your current gold is back to " << currentPlayerGold << "G. \n\n";

		}
		else if (playerRollValue > botRollValue || snakeEyes == 1) {
			cout << "You win the bet.\n";

			payoutWin(snakeEyes, playerBet, currentPlayerGold);
			snakeEyes = 0;

			cout << "Your current gold is now " << currentPlayerGold << "G. \n\n";
		}
		else if (playerRollValue < botRollValue) {
			cout << "You lose the bet. You don't earn any money.\n";
			cout << "Your current gold is still " << currentPlayerGold << "G. \n\n";


		}
	}
}

void playRound(int& goldOnHand, bool& ongoing, string answer) {
	if (goldOnHand == 1000) {
		cout << "What would you like to do? (please type your answer)\n\n      gamble       leave\n\n";
		cin >> answer;

		if (answer == "gamble") {
			system("cls");
			bet(goldOnHand);
		}
		else {
			cout << "\nGoodbye.\n";
			ongoing = 0;
		}
	}
	else {
		cout << "Would you like to play another round? (Type 'yes' or 'no')\n";
		cin >> answer;

		if (answer == "yes") {
			system("cls"); 
			bet(goldOnHand);
		}
		else {
			cout << "\nGoodbye.\n";
			ongoing = 0;
		}
	}
}

void main() {
	srand(time(NULL));
	
	bool stillPlaying = 1;
	string playerAnswer;
	int playerGold = 1000;

	//INTRO
	cout << "Welcome to the virtual casino. You start off with 1000G to bet with.\n";
	while (playerGold != 0 && (stillPlaying == 1)) {
		playRound(playerGold, stillPlaying, playerAnswer);
	}
	cout << "Kindly leave the establishment and don't come back until you have money again. :)" << endl;
}

/*ORIGINAL CODE
void main() {
	srand(time(NULL));

	string playerAnswer;
	int playerGold = 1000,
		currentPlayerGold = playerGold;
	int dice = 0,
		playerBet,
		botRoll1, botRoll2, botValue,
		playerRoll1, playerRoll2, playerValue;
	bool snakeEyes = 0;

	//INTRO
	cout << "Welcome to the virtual casino. You start off with 1000G to bet with.\n";
	cout << "What would you like to do? (please type your answer)\n\n      gamble       leave\n\n";
	cin >> playerAnswer;

	//BETTING SECTION
	while (playerAnswer == "gamble" || playerAnswer == "yes") {
		cout << "\nHow much would you like to gamble?\n";
		cin >> playerBet;

		if (playerBet == 0 || playerBet > currentPlayerGold) {
			cout << "Invalid bet. Please try a different value that isn't 0 or higher than the current gold you have.\n\n";
		}
		else {
			currentPlayerGold -= playerBet;
			cout << "\n\nYou have betted " << playerBet << "G and will be deducted from your gold. Your current money now is " << currentPlayerGold <<
				"G. \nIf your roll is higher than the bots, you win your bet.\n\nAI is rolling dice now...";

			botRoll1 = diceRolls(dice),
			botRoll2 = diceRolls(dice);
			botValue = botRoll1 + botRoll2;

			cout << "\nThe AI's rolls are " << botRoll1 << " and " << botRoll2 << endl;
			cout << "\nPlayer is rolling now...";

			playerRoll1 = diceRolls(dice),
			playerRoll2 = diceRolls(dice);
			playerValue = playerRoll1 + playerRoll2;

			//SNAKE EYE CHECK
			if (playerRoll1 == 1 && playerRoll2 == 1) {
				snakeEyes = 1;
			}

			cout << "\nThe player's rolls are " << playerRoll1 << " and " << playerRoll2 << endl << endl;

			//WINS OR LOSSES SECTION
			if (playerValue > botValue) {
				cout << "You win the bet. ";
				if (snakeEyes == 1) {
					playerBet += playerBet;
				}
				playerBet *= 2;
				currentPlayerGold += playerBet;
				cout << " Your current gold is now " << currentPlayerGold << "G. \n";
				cout << "Would you like to gamble again? (Type 'yes' or 'no')\n";
				cin >> playerAnswer;
			}
			else {
				cout << "You lose the bet. You don't earn any money.\n";
				cout << "Would you like to gamble again? (Type 'yes' or 'no')\n";
				cin >> playerAnswer;

			}

		}
	}
	cout << "\nGoodbye.\n\n";


*/