#pragma once
#include <string>
#include <iostream>
using namespace std;

class Wizard {
public:
	string mName;
	int mHP = 250,
		mMinMP = 10,
		mMaxMP = 20,
		mMP, //wizard's bar of MP
		mMPGain,
		mMinAtkDmg = 10,
		mMaxAtkDmg = 15,
		mDmgDealt;

	//pass the opposing wizard as the enemy target
	void attack(Wizard* opponent);
};