#pragma once
#include <string>
#include <iostream>
#include "Wizard.h"
using namespace std;

class Spell {
public:
	string mName;
	int mMinDmg = 40,
		mMaxDmg = 60,
		mDmgDealt, 
		mMPCost = 50;

	void activate(Wizard* opponent, Wizard* caster);
};

