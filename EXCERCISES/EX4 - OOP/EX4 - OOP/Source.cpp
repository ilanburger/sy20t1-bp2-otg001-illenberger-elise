#include <iostream>
#include <string>
#include <time.h>
#include "Wizard.h"
#include "Spell.h"

using namespace std; 

void Wizard::attack(Wizard* opponent) {
	this->mDmgDealt = rand() % (this->mMaxAtkDmg - this->mMinAtkDmg + 1) + this->mMinAtkDmg; //generate random dmg 
	opponent->mHP -= this->mDmgDealt; //health decreases from dmg dealt
	cout << mName << " attacked " << opponent->mName << " with their staff. " << mDmgDealt << " damage dealt to your opponent!\n\n";

	this->mMPGain = rand() % (this->mMaxMP - this->mMinMP) + this->mMinMP; //generate random MP gained
	this->mMP += this->mMPGain;
	cout << mName << " gained " << mMPGain << "MP from their attack.\n\n";
}

void Spell::activate(Wizard* opponent, Wizard* caster) {
	mDmgDealt = rand() % (mMaxDmg - mMinDmg + 1) + mMinDmg; //generate random spell dmg
	opponent->mHP -= mDmgDealt; //health decreases from dmg dealt
	cout << caster->mName << " activated their spell: " << this->mName << " and casted it on " << opponent->mName << "." << mDmgDealt << " damage dealt to your opponent!\n\n";

	caster->mMP -= mMPCost; //decrease MP bar 
}

void printWizardCompleteStats(Wizard* character1, Wizard* character2, Spell* character1Spell, Spell* character2Spell) {
	cout << "=========================================================\n";
	cout << "CHALLENGER 1:\n\n";
	cout << "Name: " << character1->mName << endl; 
	cout << "HP: " << character1->mHP << endl;
	cout << "MP: " << character1->mMP << endl;
	cout << "Attack damage range: " << character1->mMinAtkDmg << " - " << character1->mMaxAtkDmg << endl;
	cout << "MP Gainable: " << character1->mMinMP << " - " << character1->mMaxMP << endl << endl;
	cout << "SPELL NAME: " << character1Spell->mName << endl;
	cout << "Spell damage range: " << character1Spell->mMinDmg << " - " << character1Spell->mMaxDmg << endl << endl;
	cout << "MP Cost: " << character1Spell->mMPCost << endl << endl; 
	cout << "=========================================================\n\n"; 

	cout << "=========================================================\n";
	cout << "CHALLENGER 2:\n\n";
	cout << "Name: " << character2->mName << endl;
	cout << "HP: " << character2->mHP << endl;
	cout << "MP: " << character2->mMP << endl;
	cout << "Attack damage range: " << character2->mMinAtkDmg << " - " << character2->mMaxAtkDmg << endl;
	cout << "MP Gainable: " << character2->mMinMP << " - " << character2->mMaxMP << endl << endl;
	cout << "SPELL NAME: " << character2Spell->mName << endl;
	cout << "Spell damage range: " << character2Spell->mMinDmg << " - " << character2Spell->mMaxDmg << endl << endl;
	cout << "MP Cost: " << character2Spell->mMPCost << endl << endl;
	cout << "=========================================================\n\n";

	system("pause");
	system("cls");
}

void printCurrentStats(Wizard* wizard1, Wizard* wizard2) {
	cout << "=========================================================\n";
	cout << "CHALLENGER 1:\n\n";
	cout << "Name: " << wizard1->mName << endl;
	cout << "HP: " << wizard1->mHP << endl;
	cout << "MP: " << wizard1->mMP << endl;
	cout << "=========================================================\n";

	cout << "=========================================================\n";
	cout << "CHALLENGER 2:\n\n";
	cout << "Name: " << wizard2->mName << endl;
	cout << "HP: " << wizard2->mHP << endl;
	cout << "MP: " << wizard2->mMP << endl;
	cout << "=========================================================\n\n";

	system("pause");
	system("cls");
}

void beginBattle(Wizard* wizard1, Wizard* wizard2, Spell* spell1, Spell* spell2) {
	int round = 1; 
	
	while (wizard1->mHP > 0 && wizard2->mHP > 0) {
		cout << "=========================================================\n";
		cout << "ROUND " << round << endl << endl; 
		
		cout << "It's " << wizard1->mName << " turn.\n"; //wiz1 initiate attack
		if (wizard1->mMP >= 50) {
			//check if MP is enough to cast spell else, do regular attack. 
			spell1->activate(wizard2, wizard1);
		}
		else {
			wizard1->attack(wizard2);
		}
		if (wizard2->mHP <= 0) {
			cout << endl << endl;
			break; //break loop if health is at/ past 0
		}

		cout << "=========================================================\n";

		cout << "It's " << wizard2->mName << " turn.\n";
		if (wizard2->mMP >= 50) {
			//check if MP is enough to cast spell else, do regular attack. 
			spell1->activate(wizard1, wizard2);
		}
		else {
			wizard2->attack(wizard1);
		}
		if (wizard1->mHP <= 0) {
			cout << endl << endl;
			break; //break loop if health is at/ past 0
		}
		cout << "=========================================================\n\n\n";
		round++; 	

		printCurrentStats(wizard1, wizard2);
	}
	//when someone loses all health it will proceed to this line
	cout << "Looks like someone has lost the will to fight, already.\nThe battle will end here. Time to see who won.\n\n";

	delete spell1; //deallocate these since they wont be used anymore
	delete spell2;

	system("pause");
	system("cls");
}

void determineWinner(Wizard* wizard1, Wizard* wizard2) {
	if (wizard1->mHP > 0) {
		delete wizard2; //deallocate loser
		cout << wizard1->mName << " is the champion of the colosseum! Congratulations for managing to survive.\n";
		delete wizard1; //deallocate winner after announcement.
	}
	else {
		delete wizard1;
		cout << wizard2->mName << " is the champion of the colosseum! Congratulations for managing to survive.\n";
		delete wizard2;
	}
}


int main() {
	srand(time(0));

	cout << "Welcome to the colossuem, spectator. Two new challengers are about to appear, please name them.\n\n";
	cout << "The first fighter has arrived, what will their name be?\n";

	//instantiate the first wizard + spell, allocate it dynamically
	Wizard* wizard1 = new Wizard(); 
	Spell* spell1 = new Spell();
	cin >> wizard1->mName; //let user name them. 

	cout << "You have named them " << wizard1->mName << ".\n\nWhat about the spell title?\n"; 
	cin >> spell1->mName; //let user name wiz's spell.
		
	cout << "\nThe second fighter is about to come up the stage, what will their name be?\n";

	//instantiate the second wizard + spell, allocate it dynamically
	Wizard* wizard2 = new Wizard();
	Spell* spell2 = new Spell();
	cin >> wizard2->mName; 

	cout << "You have named them " << wizard2->mName << ".\n\nWhat about the spell title?\n";
	cin >> spell2->mName; //let user name wiz's spell.

	system("pause");
	system("cls");

	printWizardCompleteStats(wizard1, wizard2, spell1, spell2); //print out each wiz's stats

	cout << "Let's proceed to watch how this battle plays out.\n\n"; 

	beginBattle(wizard1, wizard2, spell1, spell2); //start fight

	determineWinner(wizard1, wizard2);

}
