#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

vector<string> fillUpInventory(vector<string>& inventory, const vector<string> items) {
	for (int i = 0; i < 10; i++) {
		inventory.push_back(items[rand() % 4]);
	}
	return inventory;
}

void printInventory(const vector<string> inventory) {
	for (int i = 0; i < inventory.size(); i++) {
		cout << "You have a " << inventory[i] << "." << endl;
	}

	cout << "A total of " << inventory.size() << " items in your bag.";
}

int countItemInstances(const vector<string> inventory, const vector<string> items, int specificItem, int instance) {
	//this condition will loop thru every index of 'inventory' to check.

	for (int i = 0; i < inventory.size(); i++) {
		if (items[specificItem] == inventory[i]) {
			instance++;
		}
	}
	return instance;
}

void removeItem(vector<string>& inventory, string answer) {
	for (int i = 0; i < inventory.size(); i++) {
		if (inventory[i] == answer) {
			inventory.erase(inventory.begin() + i);
			break;;
		}
	}
}

void main() {
	srand(time(NULL));

	vector<string> inventory;
	vector<string> items = { "RedPotion", "Elixir", "EmptyBottle", "BluePotion" };
	int instance = 0,
		specificItem;
	string answer;

	cout << "Hello adventurer, it seems like you are about to embark on a quest. Let me fill up your inventory for you." << endl << endl;

	//populates the inventory with the array items
	fillUpInventory(inventory, items);

	//print out all the items in inventory vector
	printInventory(inventory);
	cout << endl << endl;

	//runs thru each list of the available items
	for (specificItem = 0;  specificItem < 4; specificItem++ ) {
		//checks how many instances of mentioned item
		cout << "There are " << countItemInstances(inventory, items, specificItem, instance) << " " << items[specificItem]
			<< " in the inventory." << endl;
	}

	cout << endl << "Woud you like to remove an item from your inventory? (Type yes or no)" << endl;
	cin >> answer;

	if (answer == "yes") {
		cout << "What would you like removed? (Type the item as is)" << endl;
		cout << "RedPotion	Elixir	EmptyBottle	BluePotion" << endl << endl;
		cin >> answer;

		cout << endl;

		system("pause");
		system("cls");

		//start removal process here.
		removeItem(inventory, answer);

		//prints out the current items in inventory once more to inform reader.
		printInventory(inventory);
		cout << endl << endl;

		for (specificItem = 0; specificItem < 4; specificItem++) {
			//checks how many instances of mentioned item
			cout << "There are " << countItemInstances(inventory, items, specificItem, instance) << " " << items[specificItem]
				<< " in the inventory." << endl << endl;
		}

	}
	else {
		cout << "Okay, have fun with your journey, adventurer. Farewell and good luck.";
	}

}
