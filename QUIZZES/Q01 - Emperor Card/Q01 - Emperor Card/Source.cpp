#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

void wagerAmount(int& mmBet, const int mmLeft) {
	bool betValid = 0;

	//will keep looping until player inputs valid bet
	while (betValid == 0) {
		cout << "Now...how many millimeters will you be waging on before your doom?\n";
		cin >> mmBet;

		//checks if the mm betted on is valid
		if (mmBet > mmLeft) {
			cout << "Silly, that's not how it works. Your bet can't be higher than what's left of it.\n\n";
		}
		else if (mmBet == 0) {
			cout << "Your bet should be at least 1mm or it's not fair for either for us, no?\n\n";
		}
		else {
			betValid = 1;
		}
	}

	cout << endl << "You have betted " << mmBet << "mm. Now assigning a deck for you to play...\n\n";
	betValid = 0; //resets boolean
}

void assignDeck(vector<string>& playerHand, vector<string>& botHand, bool& slaveBonus, int round) {
	vector<string> importantCard = { "Emperor", "Slave" };

	// 0 for emperor, 1 for slave based on importantCard's index while 0 or 1 remainder for whose turn to get E or S deck. 
	if ((round > 0 && round < 4) || (round > 6 && round < 10)) {
		playerHand.push_back(importantCard[0]);
		botHand.push_back(importantCard[1]);
		slaveBonus = 0; //makes sure to set x5 off in the next 3 rounds after slave deck
		cout << "You have the Emperor deck.";
	}
	else if ((round > 3 && round < 7) || round > 9) {
		playerHand.push_back(importantCard[1]);
		botHand.push_back(importantCard[0]);

		//turns on the condition for slaveBonus (payout: 500,000)
		slaveBonus = 1; //turns on the x5 multiplier
		cout << "You have the Slave deck.";
	}
}

void printUserCards(const vector<string> playerHand) {
	for (int i = 0; i < playerHand.size(); i++) {
		cout << i << ". " << playerHand[i] << "." << endl;
	}
	cout << endl;
}

string userPlayCard(const vector<string> playerHand, int& cardIndex, string& playedCard) {
	cout << "(It's your turn! Pick a card to play from your hand. Type the number to play it.)\n\n";

	//prints the contents of user's hand from the playerHand's vector. 
	printUserCards(playerHand);

	cin >> cardIndex;

	cout << endl << endl << "(You have chosen " << playerHand[cardIndex] << " as your playing card.)" << endl << endl;
	return playedCard = playerHand[cardIndex];
}

string botPlayCard(const vector<string> botHand, int& botCardIndex, string& botPlayedCard, bool slaveBonus) {
	//determines which card the bot will play thru rng 	
	botCardIndex = rand() % botHand.size();

	return botPlayedCard = botHand[botCardIndex];
}

void deletePlayedCard(vector<string>& playerHand, vector<string>& botHand, const int botCardIndex, const int cardIndex) {
	//deletes the card played after both sides have chosen
	playerHand.erase(playerHand.begin() + cardIndex);
	botHand.erase(botHand.begin() + botCardIndex);
	cout << "You have finished playing this card, it will now be discarded." << endl;
}


void matchUp(const string playedCard, const string botPlayedCard, bool& win, bool& draw) {
	//evaluates the cards played from both sides to determine if player wins (hence the boolean win/draw) the round or not
	if (playedCard == "Citizen" && botPlayedCard == "Citizen") {
		draw = 1;
		cout << "Both you and your opponent drew a Citizen. It's a tie.";
	}
	else if (playedCard == "Citizen" && botPlayedCard == "Emperor") {
		cout << "Citizen bows down to the Emperor. Your opponent wins, too bad the drill inches closer to destroying your beloved hearing.";
	}
	else if (playedCard == "Citizen" && botPlayedCard == "Slave") {
		win = 1;
		cout << "Citizen beats Slave! You won. Will your luck last though?";
	}
	else if (playedCard == "Slave" && botPlayedCard == "Emperor") {
		win = 1;
		cout << "Slave beats Emperor! You've got nothing to lose but you still won. Nice.";
	}
	else if (playedCard == "Slave" && botPlayedCard == "Citizen") {
		cout << "Slave is overpowered by Citizens. Your opponent wins, the drill inches ever closer.";

	}
	else if (playedCard == "Emperor" && botPlayedCard == "Citizen") {
		win = 1;
		cout << "Emperor beats Citizen. You get to order around your consorts, you win!";
	}
	else if (playedCard == "Emperor" && botPlayedCard == "Slave") {
		cout << "Emperor is overthrown by the Slave. You lost. The drill whirs near towards your eardrums.";
	}
	cout << endl;
}

void playRound(int& round, int& moneyEarned, int& mmBet, int& mmLeft) {

	bool slaveBonus = 0, //grants a x5 multiplier to payout
		playerFirst = 0, botFirst = 0, //these booleans determine which player gets the emperor/slave card in the assignDeck()
		specialCardPlayed = 0, //when off, prevents code from moving to the next round
		win = 0, draw = 0; //lets the code know whether player has won or it was a tie

	vector<string> playerHand;
	vector<string> botHand;
	string playedCard, botPlayedCard;
	int cardIndex, botCardIndex;

	playerHand = { "Citizen", "Citizen", "Citizen", "Citizen" };
	botHand = { "Citizen", "Citizen", "Citizen", "Citizen" };

	system("pause");
	system("cls");

	cout << "===================================================" << endl;
	cout << "ROUND " << round << endl;
	cout << "===================================================" << endl;

	wagerAmount(mmBet, mmLeft);

	assignDeck(playerHand, botHand, slaveBonus, round);
	cout << endl << endl << endl;

	system("pause");
	system("cls");

	for (int i = 0; i < 5; i++) {
		//for loop prevents the cards from resetting entirely with the conditional statement specialcardPlayed.
		//assigns a card of player's choosing from the function
		userPlayCard(playerHand, cardIndex, playedCard);

		//assigns random card for bot from the function
		botPlayCard(botHand, botCardIndex, botPlayedCard, slaveBonus);

		matchUp(playedCard, botPlayedCard, win, draw);
		cout << endl;

		//deletes the played card after bot shows their played card
		deletePlayedCard(playerHand, botHand, cardIndex, botCardIndex);

		system("pause");
		system("cls");

		cout << "===================================================" << endl;
		cout << "ROUND " << round << endl;
		cout << "===================================================" << endl << endl;
		cout << "This turn concludes as follows: " << endl;
		cout << "===================================================" << endl << endl;

		if (win == 1) {
			if (slaveBonus == 1) {
				moneyEarned += mmBet * 500000;
				slaveBonus = 0; //turns off the slave's bonus for the next round 
			}
			else {
				moneyEarned += mmBet * 100000;
			}
			cout << "You betted " << mmBet << "mm and you won the turn earlier so you've earned " << moneyEarned << "!" << endl;
			cout << "No deductions made from your remaining " << mmLeft << "mm, on to the next round.";
			win = 0; //turns off the win from the current round
			specialCardPlayed = 1; //when this is turned on, it allows for the code to move on to the next round
		}
		else if (draw == 1) {
			cout << "It was a draw for that turn so play another card. You still have " << mmLeft << "mm to bet on." << endl;
			draw = 0;
			specialCardPlayed = 0;
		}
		else {
			mmLeft -= mmBet;
			cout << "You lost. Sorry, you know the rules and so do I." << endl << "(The drill activates and closes the distance of your " << mmBet
				<< "mm bet.) There is only " << mmLeft << "mm left you can bet on next.";
			specialCardPlayed = 1;
		}

		system("pause");
		system("cls");

		if (specialCardPlayed == 1) {
			round++;
			break;;
		}
		else if (specialCardPlayed == 0) {
			cout << "We will only end the round if either one of you plays an Emperor or a Slave.";
		}
	}
}

int main() {
	srand(time(NULL));

	int mmBet, mmLeft = 30,
		round = 1,
		moneyEarned = 0;

	//INTRO FOR PLAYER
	cout << "Welcome to the casino once again.\n\n" <<
		"I see you've no money with you--not a problem. We can wager on your ear instead." << endl <<
		"Today we'll be playing Emperor of Cards. This game has 3 special units namely: EMPEROR, CITIZEN, SLAVE." << endl << endl <<
		"The power dynamic between these 3 goes like this: " << endl << endl << "EMPEROR beats CITIZENS but loses to SLAVE" << endl <<
		"CITIZENS beats SLAVE but loses to EMPEROR" << endl << "SLAVE beats EMPEROR but loses to CITIZENS" << endl << endl;
	cout << "You'll be betting on how many millimeters before the drill pierces you ear.\n" <<
		"If you lose the drill inches closer, if you win, you'll get 100k per millimeter you betted on. \n";
	cout << endl << "..." << endl << "..." << endl << "..afraid?" << endl;
	cout << "Don't worry, you have about 30mm to bet on before it reaches your ear." << endl <<
		"Each round ends when an EMPEROR or SLAVE card is played and the game lasts for only 12 rounds.\n\n";

	while (1) {

		playRound(round, moneyEarned, mmBet, mmLeft);

		if (mmLeft == 0 || moneyEarned >= 20000000 || round > 12) {
			break;; //breaks it from the sequence when one of these conditions are met
		}
	}

	// PLAYER VICTORY OUTCOME? 
	cout << "Looks like the game has ended. And..." << endl << "..." << endl << "..." << endl << "..." << endl;

	if (moneyEarned >= 20000000 && mmLeft > 0) {
		cout << "Good job. You survived and even got the entire 20 million you needed." << endl << endl;
		cout << "YOU GOT THE BEST ENDING.";
	}
	else if (moneyEarned < 20000000 && mmLeft > 0) {
		cout << "Well...you're alive but you didn't get the exact amound you needed, good luck with that. Goodbye." << endl << endl;
		cout << "YOU GOT THE MEH ENDING.";
	}
	else if (mmLeft <= 0) {
		cout << "You're alive but you lost your hearing, awww..are you in pain? Don't worry you'll learn to live with it." << endl;
		cout << "YOU GOT THE BAD ENDING.";
	}

	cout << endl;
	return 0;
}