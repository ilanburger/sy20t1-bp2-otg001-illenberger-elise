#include <iostream>
#include <string>
#include <time.h>
#include "Node.h"

using namespace std;

/*void inputNames(Node* head, Node* temporaryPtr) {
	for (int i = 1; i < 6; i++) {
		// 5 rn is the current number of members.

		temporaryPtr = new Node;

		cin >> temporaryPtr->name;

		if (head == 0) {
			//when firstentry is still pointing to nothing, put smth.
			head = temporaryPtr;
		}
		else {
			temporaryPtr->next = head;
			head = temporaryPtr; //this pushes the last entry to the top
		}
	}
}*/ //unable to pass variable for some reason

Node* inputNameAtTail(Node* entry, const int rosterCount) {
	Node* insertTail = 0; //insert tail is the last node 

	for (int i = 0; i < rosterCount; i++) {
		// create a temp to store the last node and set the last/preceding node's data and succeeding/next node's 
		insertTail = new Node;
		cin >> insertTail->name;

		// If the linked list/head is empty or if this is the first entry then set 'head' = last node's 
		if (entry == 0) {
			entry = insertTail;
		}
		else {
			//create new temp node and hold 'head's address
			Node* temporary = new Node;

			temporary = entry;

			// temp is used here to traverse the list and look for the end
			while (temporary->next != 0) {
				temporary = temporary->next;
			}

			temporary->next = insertTail; // attach the last node with  the new last
			//insertTail->previous = temporary; //attach the new last node's prevlink with the temporary? NOT SURE
		}
	}
	return entry;
}

void printMemberList(Node* head, const int round) {
	Node* temporary1 = head;

	//if (round == 1) {
	//	do {
	//		//as long as it isnt at the NULL/end, keep printing
	//		cout << temporary1->name << endl;
	//		temporary1 = temporary1->next;
	//	} while (temporary1 != 0);
	//}
	//else {
	do {
		//as long as it isnt at the head yet, keep printing
		cout << temporary1->name << endl;
		temporary1 = temporary1->next;
	} while (temporary1 != head);
	//}
}

int generateRandomNum(int& roll, const int rosterCount) {
	return roll = rand() % rosterCount + 1;
	//note to self: when returning simple things like this dont forget to assign the variable to the return :o 
}

Node* commanderPick(Node* head, const int commanderRoll) {
	int counter = 1;

	Node* temporary2 = head;

	while (temporary2 != 0) {
		if (counter == commanderRoll) {
			//if counter reaches the RNG, return the node corresponding to it
			return temporary2;
		}
		temporary2 = temporary2->next;
		counter++;
	}
	return 0;
}

Node* linkTailAndHead(Node* memberList) {
	Node* temporary = new Node; //create new temp to hold the member list for this function 
	temporary = memberList;

	while (temporary->next != 0) {
		temporary = temporary->next; //transverses the list to look for the end.
	}

	temporary->next = memberList; //links the last node to head.
	//memberList->previous = temporary; //links head prevlink to last node. NOT SURE

	return memberList;
}

Node* eliminatePick(Node* toDelete, const int memberRoll) {
	//toDelete is the passed node of member called and starts from there to traverse list till u find member to kick.
	Node* tempPrevious = 0; //make temp to store the previous node so we can reconnect it to the node after.

	for (int i = 0; i < memberRoll; i++) {
		tempPrevious = toDelete; //keep track of previous node 
		toDelete = toDelete->next;  //move headptr to next node 
	}

	tempPrevious->next = toDelete->next;  //link the cut nodes before deleting
	toDelete->previous = tempPrevious->previous; //NOT SURE

	return toDelete;
}

void passAround(int& rosterCount, Node*& nameEntry) {
	int memberRoll, commanderRoll,
		rounds = rosterCount;
	Node* memberList = nameEntry;
	Node* tempHead = 0;
	memberList = linkTailAndHead(memberList); //turn list into circular linked list.


	for (int round = 1; round < rounds; round++) {
		cout << "===================================================" << endl;
		cout << "ROUND " << round << ":" << endl;
		cout << "===================================================" << endl;

		cout << "Your current roster of active members are..." << endl;
		printMemberList(nameEntry, round); //show user the current roster

		cout << endl << endl;

		//memberList = nameEntry; //this will hold the circular list of members, will be manipulated during the elimination 

		if (round == 1) {
			cout << "You randomly choose among them who are tasked to pass around the cloak of elimination." << endl;

			generateRandomNum(commanderRoll, rosterCount);
			tempHead = commanderPick(memberList, commanderRoll); //temporaryHead represent the current head of the circular linked list
		}
		else {
			tempHead = nameEntry;
		}

		generateRandomNum(memberRoll, rosterCount);
		cout << "Soldier " << tempHead->name << " gets to pass around the cloak." << endl << "They chose " << memberRoll << "." << endl; //error is here for some reason

		Node* toDelete = eliminatePick(tempHead, memberRoll);
		cout << "Soldier " << toDelete->name << " ended up holding the cloak. They are ordered to stay behind and fight off the White Walkers." << endl;
		nameEntry = toDelete->next; //cloak is given to next person inline to start the cycle again

		delete toDelete; //deallocate
		toDelete = 0; //reset to null 

		//cout << nameEntry->name << endl; //just to check whos holding the cloak after.

		rosterCount -= 1; //decrease rosterCount after every elimination

		cout << "===================================================" << endl;
		system("pause");
		system("cls");
	}
}

int main() {
	srand(time(0));
	int rosterCount; //represents how many members are there currently 
	Node* nameEntry = 0; //head here is a just a representative of the user's entries, not the actual fixedhead

	cout << "How many members are you calling over?" << endl;
	cin >> rosterCount;  //implement the custom # of members later when rng is fixed.
	cout << "You call over the most skilled combatants, such as..." << endl;

	nameEntry = inputNameAtTail(nameEntry, rosterCount); //attaches the data gotten from function to the 'head'

	system("pause");
	system("cls");

	passAround(rosterCount, nameEntry);

	cout << "===================================================" << endl;
	cout << "The rest of the soldiers are dismissed back into battle to help assist the remaining members of the crew." << endl << endl;
	cout << "You turn to them." << endl << endl;
	cout << "Soldier " << nameEntry->name << " you are in charge of sending out the message. Godspeed." << endl;
	cout << "===================================================" << endl << endl;


	return 0;
}