#pragma once
#include <iostream>
#include <string>
using namespace std;

class Unit
{
public:
	Unit(string name, string className, int hp, int power, int vitality, int agility, int dexterity); //set the initial stats of the player unit depending on the class type
	Unit(string name,string className, int hp); //set the initial stats of the enemy unit depending on the class type
	bool landHit(Unit* opponent); //determine if attack hits opponent
	void attack(Unit* opponent); 
	void statGain(Unit* opponent); //when unit is victorious against a certain class, then stat gain 
	void heal(); //heal when victorious

	//getters = when u just need to print out stuff for private variables
	string getName();
	string getClass();
	int getHP();
	int getMaxHP();
	int getPOW();
	int getVIT();
	int getAGI();
	int getDEX();
	bool checkBonus();
	// setters = when u need to update private variables
	void setHP(int value);
	void setPOW(int value);
	void setVIT(int value);
	void setAGI(int value);
	void setDEX(int value);
	string activateBonus(Unit* defender);
	void deactivateBonus(int value);

private:
	//simple stats
	string mName,
		mClassType; 
	int mHP,
		mMaxHP,
		mPOW,
		mVIT,
		mAGI,
		mDEX;
	bool mBonus = 0; //tells the unit if theyre effective against a unit
};

