#include "Unit.h"

Unit::Unit(string name, string className, int hp, int power, int vitality, int agility, int dexterity)
{
	mName = name;
	mClassType = className;
	mHP = hp;
	mMaxHP = mHP;
	mPOW = power;
	mVIT = vitality;
	mAGI = agility;
	mDEX = dexterity; 
}

Unit::Unit(string name, string className, int hp)
{
	mName = name;
	mClassType = className;
	mHP = hp;
	mMaxHP = mHP;
}

bool Unit::landHit(Unit* opponent)
{
	float hitrate = (this->mDEX / opponent->mAGI) * 100;
	// the if statements cap it off if the hitrate is more than 80 ir less than 20
	if (hitrate > 80) {
		hitrate = 80;
	}
	if (hitrate < 20) {
		hitrate = 20;
	}

	return rand() % 100 < hitrate; //outputs 1 or 0/ true or false 
}

void Unit::attack(Unit* opponent)
{
	int opponentHP = opponent->getHP(); 
	int damage = (mPOW - opponent->mVIT);
	if (damage < 0) {
		damage = 1; //the lowest damage u can deal is 1. 
	}
	cout << damage << " damage!\n";
	if (this->checkBonus() == 1) {
		damage *= 1.5;
		cout << "Class bonus applied! 1.5x multiplier!\n";
	}
	opponentHP -= damage; 
	opponent->setHP(opponentHP); //update the enemy's new health 

	cout << mName << " attacked " << opponent->mName << "! A total of " << damage << " damage dealt to your opponent!\n";
	cout << opponent->mName << " has " << opponent->getHP() << "HP left.\n\n";

}

void Unit::statGain(Unit* opponent)
{
	if (opponent->mClassType == "Warrior") {
		int updatePOW = getPOW() + 3,
			updateVIT = getVIT() + 3; 
		setPOW(updatePOW);
		setVIT(updateVIT);
		cout << mName << " gained +3 to their POWER and VITALITY!\n\n";
	}
	else if (opponent->mClassType == "Assassin") {
		int updateAGI = getAGI() + 3,
			updateDEX = getDEX() + 3;
		setAGI(updateAGI);
		setDEX(updateDEX);
		cout << mName << " gained +3 to their AGILITY and DEXTERITY!\n\n";

	}
	else if (opponent->mClassType == "Mage") {
		int updatePOW = getPOW() + 5,
		setPOW(updatePOW);
		cout << mName << " gained +3 to their POWER!\n\n";
	}
}

void Unit::heal()
{
	int hp = getHP(),
		heal = getMaxHP();
	heal *= 0.3;
	hp += heal;
	setHP(hp);
	cout << "You healed up 30% of your max health! Gained " << heal << "HP back.\n"; 
}

string Unit::getName()
{
	return mName;
}

string Unit::getClass()
{
	return mClassType;
}

int Unit::getHP()
{
	return mHP;
}

int Unit::getMaxHP()
{
	return mMaxHP;
}

int Unit::getPOW()
{
	return mPOW;
}

int Unit::getVIT()
{
	return mVIT;
}

int Unit::getAGI()
{
	return mAGI;
}

int Unit::getDEX()
{
	return mDEX;
}

bool Unit::checkBonus()
{
	return mBonus;
}

void Unit::setHP(int value)
{
	mHP = value;
	if (mHP > getMaxHP()) {
		mHP = getMaxHP();
	}
	if (mHP < 0) {
		mHP = 0; //floor the possible value to 0 only 
	}
}

void Unit::setPOW(int value)
{
	mPOW = value;

}

void Unit::setVIT(int value)
{
	mVIT = value;
}

void Unit::setAGI(int value)
{
	mAGI = value;
}

void Unit::setDEX(int value)
{
	mDEX = value;
}

string Unit::activateBonus(Unit* defender)
{
	//activates the 150% bonus dmg if on 
	if (this->mClassType == "Mage" && defender->mClassType == "Warrior") {
		mBonus = 1;
	}
	else if (this->mClassType == "Assassin" && defender->mClassType == "Mage") {
		mBonus = 1;
	}
	else if (this->mClassType == "Warrior" && defender->mClassType == "Assassin") {
		mBonus = 1;
	}
	else {
		mBonus = 0; //no change
	}

	if (mBonus == 1) {
		return mClassType + " is strong against " + defender->mClassType + ". " + defender->mName + " is weak to " + mName + "!\n";
	}
	else {
		return "No particular class bonuses applicable for " + mName + ".\n";
	}
}

void Unit::deactivateBonus(int value)
{
	mBonus = value;
}
