#include <iostream>
#include <string>
#include <time.h>
#include "Unit.h"
using namespace std;

Unit* determinePlayerClass(Unit* player,string name) {
	string classAnswer,
		answer; //yes or no
	
	while (1) {
		cout << "[Mage] - a glass cannon that can one shot an enemy and has high evasion rate but is also susceptible to being one shot yourself.\n"
			<< "{Warrior] - a classic and reliable pick, this unit is tanky and have pretty balance stats.\n"
			<< "[Assassin] - deadly and quick, they deal high damage and are able to dodge most attacks.\n\n";
		cout << "I would like to play as a: ";
		cin >> classAnswer;
		cout << "\nYou have chosen the class " << classAnswer << ". Are you sure this is the class you want to pick? (type 'yes' or 'no')\n";
		cin >> answer;
		if (answer == "yes") {
			break;;
		}
		else {
			cout << "Feel free to pick again.\n\n";
		}
	}

	if (classAnswer == "Mage") {
		player = new Unit(name, classAnswer, 85, 22, 12, 23, 15);
	}
	else if (classAnswer == "Warrior") {
		player = new Unit(name, classAnswer, 150, 18, 21, 9, 14);
	}
	else if (classAnswer == "Assassin") {
		player = new Unit(name, classAnswer, 110, 23, 9, 22, 20);
	}
	return player; 
}

void printUnitStats(Unit* character) {
	cout << "=========================================================\n";
	cout << "UNIT:\n\n";
	cout << "Name: " << character->getName() << endl;
	cout << "Class: " << character->getClass() << endl;
	cout << "HP: " << character->getHP() << endl;
	cout << "POW: " << character->getPOW() << endl;
	cout << "VIT: " << character->getVIT() << endl;
	cout << "AGI: " << character->getAGI() << endl;
	cout << "DEX: " << character->getDEX() << endl;
	cout << "=========================================================\n\n";
}

int generateAttribute(string degree, const int round) {
	int	value,
		r = round; 
	double scaler = .25; //variable in charge of scaling up the enemy stats
	if (degree == "low") {
		if (round > 1) {
			value = rand() % (12 - 7 + 1) + 7;
			scaler *= r - 1;
			value += scaler * value;
			return value;
		}
		else {
			return rand() % (12 - 7 + 1) + 7;
		}
	}
	else if (degree == "avg") {
		if (round > 1) {
			value = rand() % (18 - 13 + 1) + 13;
			scaler *= r - 1;
			value += scaler * value;
			return value;
		}
		else {
			return rand() % (18 - 13 + 1) + 13;
		}
	}
	else if (degree == "high") {
		if (round > 1) {
			value = rand() % (23 - 19 + 1) + 19;
			scaler *= r - 1;
			value += scaler * value;
			return value;
		}
		else {
			return rand() % (23 - 19 + 1) + 19;
		}
	}
}

void printBattleProgress(Unit* player,int round) {
	round -= 1; 
	cout << "=========================================================\n";
	cout << "Name: " << player->getName() << endl;
	cout << "Class: " << player->getClass() << endl;
	cout << "Battles won: " << round << endl;
	cout << "=========================================================\n\n";
}

Unit* determineEnemyClass(Unit* opponent, int round) {
	string classType[] = { "Mage","Warrior","Assassin" },
		degree[] = {"low","avg","high"};
	int i, value;
	i = rand() % 3;

	if (classType[i] == "Mage") {
		opponent = new Unit("Monster", classType[i], 85);
		opponent->setPOW(generateAttribute(degree[2], round));
		opponent->setVIT(generateAttribute(degree[0], round));
		opponent->setAGI(generateAttribute(degree[2], round));
		opponent->setDEX(generateAttribute(degree[1], round));
	}
	else if (classType[i] == "Warrior") {
		opponent = new Unit("Monster", classType[i], 150);
		opponent->setPOW(generateAttribute(degree[1], round));
		opponent->setVIT(generateAttribute(degree[2], round));
		opponent->setAGI(generateAttribute(degree[0], round));
		opponent->setDEX(generateAttribute(degree[1], round));
	}
	else if (classType[i] == "Assassin") {
		opponent = new Unit("Monster", classType[i], 110);
		opponent->setPOW(generateAttribute(degree[2], round));
		opponent->setVIT(generateAttribute(degree[0], round));
		opponent->setAGI(generateAttribute(degree[2], round));
		opponent->setDEX(generateAttribute(degree[2], round));
	}
	return opponent;
}

void outcome(Unit* player, Unit* opponent, int& round) {
	cout << "=========================================================\n";
	cout << "BATTLE " << round << " RESULTS: " << endl << endl;

	if (player->getHP() > 0) {
		cout << player->getName() << " is the winner for this round! Congratulations for managing to survive, onto the next.\n";
		player->statGain(opponent);
		delete opponent; //deallocate loser
		opponent = 0; //set ptr back to null
		player->heal(); 
		system("pause");
		system("cls");
		round++;
	}
	else {
		cout << opponent->getName() << " is the winner for this round!" << player->getName() << " has been defeated in battle.\n";
		cout << "You lost. This is how far you've gotten:\n";

		printBattleProgress(player, round);
	}


}


void duel(Unit* player, Unit* opponent,int& round) {
	int turn = 1; 

	while (player->getHP() > 0 && opponent->getHP() > 0) {
		if (player->getAGI() > opponent->getAGI()) {
			//if player agi is higher then they go first
			cout << "=========================================================\n";
			cout << "BATTLE " << round << ": TURN " << turn << endl << endl;
			cout << "It's " << player->getName() << " turn.\n"; //player starts their turn

			if (player->landHit(opponent) == 1) {
				player->attack(opponent);
			}
			else {
				cout << "Miss!\n\n";
			}
			if (opponent->getHP() <= 0) {
				cout << endl << endl;
				break; //break loop if health is at/ past 0
			}
			cout << "=========================================================\n";
			cout << "It's " << opponent->getName() << " turn.\n"; //opponent's turn

			if (opponent->landHit(player) == 1) {
				opponent->attack(player);
			}
			else {
				cout << "Miss!\n\n";
			}
			if (player->getHP() <= 0) {
				cout << endl << endl;
				break;
			}
			cout << "=========================================================\n\n\n";
		}
		else {
		//opponent goes first
			cout << "=========================================================\n";
			cout << "BATTLE " << round << ": TURN " << turn << endl << endl;
			cout << "It's " << opponent->getName() << " turn.\n"; //player starts their turn

			if (opponent->landHit(player) == 1) {
				opponent->attack(player);
			}
			else {
				cout << "Miss!\n\n";
			}
			if (player->getHP() <= 0) {
				cout << endl << endl;
				break; //break loop if health is at/ past 0
			}
			cout << "=========================================================\n";
			cout << "It's " << player->getName() << " turn.\n"; //opponent's turn

			if (player->landHit(opponent) == 1) {
				player->attack(opponent);
			}
			else {
				cout << "Miss!\n\n";
			}
			if (opponent->getHP() <= 0) {
				cout << endl << endl;
				break;
			}
			cout << "=========================================================\n\n\n";
		}
		turn++;

		system("pause");
		system("cls");
	}
	player->deactivateBonus(0); //deactivate bonuses if either parties after defeat
	opponent->deactivateBonus(0);

	cout << "...\n...\n...\n";
	cout << "Someone has been bested in strife!\n";

	outcome(player, opponent, round); //determine winner
}

void next(Unit* player) {
//next match to face
	Unit* opponent = 0;
	int round = 1;

	while (player->getHP() > 0) {
		cout << "=========================================================\n";
		cout << "CHARACTER STATS:\n";
		printUnitStats(player);

		opponent = determineEnemyClass(opponent, round);

		cout << "You will be facing against this beast, behold! \nTry not to die to early...\n\n";

		printUnitStats(opponent);
		cout << player->activateBonus(opponent);
		cout << opponent->activateBonus(player); 

		system("pause");
		system("cls");

		duel(player, opponent, round);
	}
}

int main() {
	srand(time(0));
	string playerName;
	Unit* player = 0;

	cout << "=========================================================\n";
	cout << "Welcome to the arena, brave heart.\nUnderstand that once you have participated you cannot back out, it will be a battle to the death.\n";
	cout << "If you understand, please allow me to know your name, courageous soul?\n";
	cin >> playerName; 
	cout << "\nNice to meet you, " << playerName << ". Now that we're done with formalities, what class would you fight as?\n\n";
	player = determinePlayerClass(player, playerName);

	cout << "=========================================================\n\n\n";
	system("pause");
	system("cls");

	cout << "\nYou are ready now...time for you to enter the arena, here are your current stats. Godspeed to you " << player->getName() << ".\n\n";

	next(player);
}