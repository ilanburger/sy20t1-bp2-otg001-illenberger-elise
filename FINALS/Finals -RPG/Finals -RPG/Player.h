#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Unit.h"
using namespace std;

class Player : public Unit
{
public:	
	Player(string name, string job, int hp, int pow, int vit, int dex, int agi);

	void run(bool& successRun);
	void levelUp(int expQuota);
	void recover();
	void explore();

	void showStats();

	int getLevel();

	int getX();
	int getY();

	void setWins(int value);

private:
	int mLvl = 1,
		mXAxis,
		mYAxis,
		mBattlesWon; 
};

