#include "Item.h"

Item::Item(string name, int value)
{
    mName = name;
    mCost = value;
}

string Item::getName()
{
    return mName;
}

int Item::getCost()
{
    return mCost;
}
