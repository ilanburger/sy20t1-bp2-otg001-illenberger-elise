#pragma once
#include <string>
#include <iostream>
#include <vector>
using namespace std;

class Item;

class Unit
{
public:
	Unit(string name, string job, int hp, int power, int vitality, int dexterity, int agility);
	Unit(string name, string kind, int hp, int exp, int money);
	
	bool hitRate(Unit* target);
	void attack(Unit* target); 

	string getName();
	string getType(); 

	int getMaxHP();
	void setMaxHP(int value); 
	int getHP();
	void setHP(int value);

	int getPower();
	void setPower(int value);

	int getVitality();
	void setVitality(int value);

	int getDexterity();
	void setDexterity(int value);

	int getAgility();
	void setAgility(int value);

	int getMoney();
	void addMoney(int price);
	void subtractMoney(int price);


	int getExp();
	void setExp(int exp);

	void setArmor(Item* armor);
	void deleteArmor();
	string getArmorName();
	void setDef();
	int getDef();

	void setWeapon(Item* weapon);
	void deleteWeapon();
	string getWeaponName();
	void setDmg(Item* weapon);
	int getDmg();
	
private:
	string mName,
			mType;

	Item* mWeapon;
	Item* mArmor; 

	int mHP,
		mMaxHP,
		mPower,
		mVitality,
		mAgility,
		mDexterity,
		mMoney,
		mExp,
		mDef,
		mDmg;
};

