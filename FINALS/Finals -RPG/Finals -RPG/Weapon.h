#pragma once
#include <string>
#include <iostream>
#include "Item.h"
using namespace std;

class Weapon : public Item
{
public:
	Weapon(string name, int value);

	int getProperties() override;
private: 
	int mDmg;
};

