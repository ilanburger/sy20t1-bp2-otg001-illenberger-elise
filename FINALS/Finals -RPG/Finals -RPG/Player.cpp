#include "Player.h"


Player::Player(string name, string job, int hp, int pow, int vit, int dex, int agi)
	: Unit(name,job,hp,pow,vit,dex,agi)
{
}

void Player::run(bool& successRun)
{
	int success = rand() % 100 < (0.25f * 100);
	if (success == 1) {
		cout << "You run away!\n\n";
		successRun = 1;
	}
	else {
		cout << "Fail!\n\n";
	}
}

void Player::recover()
{
	int healUp = getMaxHP() - getHP(); 
	cout << "You took a long rest. You recover " << healUp << "HP back.\n";
	healUp += getHP();
	setHP(healUp);
}

void Player::explore()
{
	int answer;
	string ways[] = { "North","South","West","East" };

	cout << "You are at (" << mXAxis << ", " << mYAxis << "). (choose a number)\n";
	cout << "1. North		2. South\n" << "3. West			4. East\n";
	cin >> answer;
	answer--;

	switch (answer) {
	case 0:
		 mYAxis++;
		break;
	case 1:
		mYAxis--;
		break;
	case 2:
		mXAxis--;
		break;
	case 3:
		mXAxis++;
		break;
	}
	cout << "You went " << ways[answer] << ". (" << mXAxis << ", " << mYAxis << ").\n";
}

void Player::showStats()
{
	cout << "__________________________________________________________________________________\n";
	cout << "UNIT:\n\n";
	cout << "Name: " << getName() << endl;
	cout << "Class: " << getType() << "		Level: " << mLvl << endl;
	cout << "Experience: " << getExp() << "/" << mLvl * 1000 << "	Gold: " << getMoney() << endl << endl;

	cout << "STATS:\n";
	cout << "Weapon: " << getWeaponName() << "		Weapon Damage: " << getDmg() << endl;
	cout << "Armor: " << getArmorName() << "		Armor Defense: " << getDef() << endl;
	cout << "HP: " << getHP() << "/" << getMaxHP() << endl;
	cout << "POW: " << getPower() << "		VIT: " << getVitality() << endl;
	cout << "AGI: " << getAgility() << "		DEX: " << getDexterity() << endl << endl;
	cout << "Enemies Slayed: " << mBattlesWon << endl;
	cout << "__________________________________________________________________________________\n\n";
}

void Player::levelUp(int expQuota)
{
	int extraExp = 0;
	if (getExp() > expQuota) {
		extraExp = getExp() - expQuota;
	}
	setExp(0); //reset
	setExp(extraExp); 

	mLvl++;
	setMaxHP(rand() % (10 - 2 + 1)+2); 
	setPower(rand() % 6);
	setVitality(rand() % 6);
	setDexterity(rand() % 6);
	setAgility(rand() % 6);
	cout << "Leveled Up! Your stats increased.\n";
	showStats();
}

int Player::getLevel()
{
	return mLvl;
}

int Player::getX()
{
	return mXAxis;
}

int Player::getY()
{
	return mYAxis;
}

void Player::setWins(int value)
{
	mBattlesWon += value;
}