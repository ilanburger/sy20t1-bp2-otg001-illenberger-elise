#include <iostream>
#include <string>
#include <time.h>
#include "Unit.h"
#include "Player.h"
#include "Item.h"
#include "Weapon.h"
#include "Armor.h"


using namespace std;

Player* pickClass(Player* you, string name) {
	string job,answer;

	while (1) {
		cout << "__________________________________________________________________________________\n";
		cout << "PICK A STARTER CLASS, CHOOSE WISELY (type in the whole word.)\n";
		cout << "__________________________________________________________________________________\n";
		cout << "Warrior - HIGH POWER + BETTER STARTER WEAPON\n"
			<< "Crusader - HIGH HP & VITALITY\n"
			<< "Thief - HIGH DEX & AGI\n\n";
		cin >> job;
		if (job == "Crusader" || job == "Warrior" || job == "Thief") {
			cout << "\nYou chose " << job << ". Confirm? (yes/no)\n";
			cin >> answer;
			if (answer == "yes") {
				break;;
			}
			else {
				cout << "Choose wisely.\n\n";
			}
		}
		else if (job == "wisely" || job == "Wisely" || job== "WISELY") {
			cout << "(That's not how this works! >:( )\n";
		}
		else {
			cout << "This class doesn't exist.\n";
		}
	}
 
	 if (job == "Warrior") {
	you = new Player(name, job, 75, 23, 16, 15, 14);
	Item* noArmor = new Armor("Plain Clothes", 0 );
	Item* dullWeapon = new Weapon("Dull Claymore", 5);
	you->setArmor(noArmor);
	you->setWeapon(dullWeapon);
	}
	else if (job == "Crusader") {
		you = new Player(name, job, 120, 14, 23, 15, 13);
		Item* noArmor = new Armor("Plain Clothes", 0);
		Item* dullWeapon = new Weapon("Dull Blade", 0);
		you->setArmor(noArmor);
		you->setWeapon(dullWeapon);
	}
	else if (job == "Thief") {
		you = new Player(name, job, 75, 17, 15, 22, 21);
		Item* noArmor = new Armor("Plain Clothes", 0);
		Item* dullWeapon = new Weapon("Dull Dagger", 0);
		you->setArmor(noArmor);
		you->setWeapon(dullWeapon);
	}
	return you;
}

int enemyStats(string degree, const int lvl) {
	int	value;
	float multiplier = .25f;
	if (degree == "low") {
		if (lvl > 1) {
			value = rand() % (12 - 7 + 1) + 7;
			multiplier *= lvl - 1;
			value += multiplier * value;
			return value;
		}
		else {
			return rand() % (12 - 7 + 1) + 7;
		}
	}
	else if (degree == "avg") {
		if (lvl > 1) {
			value = rand() % (18 - 13 + 1) + 13;
			multiplier *= lvl - 1;
			value += multiplier * value;
			return value;
		}
		else {
			return rand() % (18 - 13 + 1) + 13;
		}
	}
	else if (degree == "high") {
		if (lvl > 1) {
			value = rand() % (23 - 19 + 1) + 19;
			multiplier *= lvl - 1;
			value += multiplier * value;
			return value;
		}
		else {
			return rand() % (23 - 19 + 1) + 19;
		}
	}
}

bool chance() {
	return rand() % 100 < (0.20f * 100);
}

Unit* hellSpawn(Unit* opponent, Player* you) {
	int rng, i;
	rng = rand() % 80 + 1; //add the probabilities of the remaining chances(?)

	if (rng > 5) {
		//for goblins, ogre & orc respectively
		i = rand() % 3; 
		if (i == 0) {
			opponent = new Unit("Goblin", "Goblin", 25, 100, 10);
			opponent->setPower(enemyStats("low", you->getLevel()));
			opponent->setVitality(enemyStats("low", you->getLevel()));
			opponent->setAgility(enemyStats("low", you->getLevel()));
			opponent->setDexterity(enemyStats("low", you->getLevel()));
		}
		else if (i == 1) {
			opponent = new Unit("Ogre", "Ogre", 50, 250, 50);
			opponent->setPower(enemyStats("avg", you->getLevel()));
			opponent->setVitality(enemyStats("low", you->getLevel()));
			opponent->setAgility(enemyStats("low", you->getLevel()));
			opponent->setDexterity(enemyStats("avg", you->getLevel()));
		}
		else {
			opponent = new Unit("Orc", "Orc", 80, 500, 100);
			opponent->setPower(enemyStats("avg", you->getLevel()));
			opponent->setVitality(enemyStats("avg", you->getLevel()));
			opponent->setAgility(enemyStats("avg", you->getLevel()));
			opponent->setDexterity(enemyStats("avg", you->getLevel()));
		}
	}
	else {
		opponent = new Unit("Orc Lord", "Orc Lord", 150, 1000, 1000);
		opponent->setPower(enemyStats("high", you->getLevel()));
		opponent->setVitality(enemyStats("high", you->getLevel()));
		opponent->setAgility(enemyStats("low", you->getLevel()));
		opponent->setDexterity(enemyStats("avg", you->getLevel()));
	}

	return opponent;
}

void battleResult(Player* you, Unit* opponent) {
	int expQuota = you->getLevel() * 1000;
	
	cout << "__________________________________________________________________________________\n";
	cout << "BATTLE RESULTS: " << endl;

	if (you->getHP() > 0) {
		cout << you->getName() << " won the battle!\n";
		cout << "__________________________________________________________________________________\n";
		you->setWins(1);
		cout << "Monster dropped " << opponent->getMoney() << "G!\n";
		you->addMoney(opponent->getMoney());
		cout << "You get " << opponent->getExp() << "EXP!\n";
		you->setExp(opponent->getExp());
		cout << "__________________________________________________________________________________\n";

		if (you->getExp() >= expQuota) {
			you->levelUp(expQuota);
		}
		delete opponent; 
		opponent = 0;
		system("pause");
		system("cls");
	}
	else {
		cout << opponent->getName() << " mutilated " << you->getName() << ".\n";
		cout << "__________________________________________________________________________________\n";
		cout << "You died. Character results:\n";
		you->showStats(); 
	}
}

void fight(Player* you, Unit* opponent) {
	int turn = 1,
		answer;
	bool run = 0; 

	while (you->getHP() > 0 && opponent->getHP() > 0 && run == 0) {
		if (you->getAgility() > opponent->getAgility()) {
			cout << "__________________________________________________________________________________\n";
			cout << "FIGHT!!!\n";
			cout << "__________________________________________________________________________________\n";
			cout << "TURN" << turn << endl;
			cout << "__________________________________________________________________________________\n"; 
			cout << you->getName() << "'s TURN"<< endl;
			cout << "__________________________________________________________________________________\n";
			cout << "1. Fight		2. Run\n";
			cin >> answer;

			if (answer == 1) {
				if (you->hitRate(opponent) == 1) {
					you->attack(opponent);
				}
				else {
					cout << "Miss!\n\n";
				}
				if (opponent->getHP() <= 0) {
					cout << endl << endl;
					break; 
				}

				cout << "__________________________________________________________________________________\n";
				cout << opponent->getName() << "'s TURN\n";
				cout << "__________________________________________________________________________________\n";


				if (opponent->hitRate(you) == 1) {
					opponent->attack(you);
				}
				else {
					cout << "Miss!\n\n";
				}
				if (you->getHP() <= 0) {
					cout << endl << endl;
					break;
				}
				cout << "__________________________________________________________________________________\n\n\n";
			}
			else {
				you->run(run); 
				if (run == 0) {
					cout << "__________________________________________________________________________________\n";
					cout << opponent->getName() << "'s TURN\n";
					cout << "__________________________________________________________________________________\n";


					if (opponent->hitRate(you) == 1) {
						opponent->attack(you);
					}
					else {
						cout << "Miss!\n\n";
					}
					if (you->getHP() <= 0) {
						cout << endl << endl;
						break;
					}
					cout << "__________________________________________________________________________________\n\n\n";
				}
			}
		}
		else {
			cout << "__________________________________________________________________________________\n";
			cout << "FIGHT!!!\n";
			cout << "__________________________________________________________________________________\n";
			cout << "TURN" << turn << endl << endl;
			cout << "__________________________________________________________________________________\n";
			cout << opponent->getName() << "'s TURN\n"; 
			cout << "__________________________________________________________________________________\n";
			if (opponent->hitRate(you) == 1) {
				opponent->attack(you);
			}
			else {
				cout << "Miss!\n\n";
			}
			if (you->getHP() <= 0) {
				cout << endl << endl;
				break;
			}
			cout << "__________________________________________________________________________________\n";
			cout << "It's " << you->getName() << "'s turn.\n What do you want to do?\n";
			cout << "1. Fight		2. Run\n";
			cin >> answer;

			if (answer == 1) {
				if (you->hitRate(opponent) == 1) {
					you->attack(opponent);
				}
				else {
					cout << "Miss!\n\n";
				}
				if (opponent->getHP() <= 0) {
					cout << endl << endl;
					break;
				}
			}
			else {
				you->run(run);
				if (run == 0) {
					cout << "__________________________________________________________________________________\n";
					cout << opponent->getName() << "'s TURN\n";
					cout << "__________________________________________________________________________________\n";


					if (opponent->hitRate(you) == 1) {
						opponent->attack(you);
					}
					else {
						cout << "Miss!\n\n";
					}
					if (you->getHP() <= 0) {
						cout << endl << endl;
						break;
					}
					cout << "__________________________________________________________________________________\n\n\n";
				}
			}
			cout << "__________________________________________________________________________________\n\n\n";
		}
		turn++;
		system("pause");
		system("cls");
	}

	if (run == 0) {
		cout << "A body falls to the ground!\n";
		battleResult(you, opponent); 
	}
}

void shop(Player* you) {
	Item* sSword = new Weapon("Short Sword", 10);
	Item* lSword = new Weapon("Long Sword", 50);
	Item* bSword = new Weapon("Broad Sword", 200);
	Item* excalibur = new Weapon("Excalibur", 9999);
	Item* lMail = new Armor("Leather Mail", 50);
	Item* cMail = new Armor("Chain Mail", 100);
	Item* pArmor = new Armor("Plate Armor", 300);

	int pick, buy;
	bool shopping = 1;

	while (shopping == 1) {
		cout << "__________________________________________________________________________________\n";
		cout << "SHOPKEEPER: Welcome to the equipment store. Would you like to buy something.\n" <<
			"(Current gold:" << you->getMoney() << ")\n";
		cout << "__________________________________________________________________________________\n";
		cout << "1. Checkout Weapons\n2. Checkout Armor\n3. Exit\n";
		cin >> pick;

		system("pause");
		system("cls");

		switch (pick) {
		case 1:
			cout << "__________________________________________________________________________________\n";
			cout << "SHOPKEEPER: These swords will give you better damage. (pick a number)\n";
			cout << "__________________________________________________________________________________\n";
			cout << "1. Short Sword - 5DMG+ (10G)\n" <<
					"2. Long Sword - 10DMG+ (50G)\n" <<
					"3. Broad Sword - 20DMG+ (200G)\n" <<
					"4. Excalibur - 999DMG+ (9999G)\n" <<
					"5. Back\n";
			cin >> buy;

			if (buy == 1) {
				if (you->getMoney() >= sSword->getCost()) {
					you->deleteWeapon();
					you->setWeapon(sSword);
					you->subtractMoney(sSword->getCost());
					cout << "You got and wore the " << sSword->getName() << ".\n";
				}
				else if (you->getMoney() < sSword->getCost()){
					cout << "__________________________________________________________________________________\n";
					cout << "SHOPKEEPER: You don't have enough.\n";
					cout << "__________________________________________________________________________________\n\n";
				}
			}
			else if (buy == 2) {
				if (you->getMoney() >= lSword->getCost()) {
					you->deleteWeapon();
					you->setWeapon(lSword);
					you->subtractMoney(lSword->getCost());
					cout << "You got and wore the " << lSword->getName() << ".\n";
				}
				else if (you->getMoney() < lSword->getCost()) {
					cout << "__________________________________________________________________________________\n";
					cout << "SHOPKEEPER: You don't have enough.\n";
					cout << "__________________________________________________________________________________\n\n";
				}
			}
			else if (buy == 3) {
				if (you->getMoney() >= bSword->getCost()) {
					you->deleteWeapon();
					you->setWeapon(bSword);
					you->subtractMoney(bSword->getCost());
					cout << "You got and wore the " << bSword->getName() << ".\n";
				}
				else if (you->getMoney() < bSword->getCost()) {
					cout << "__________________________________________________________________________________\n";
					cout << "SHOPKEEPER: You don't have enough.\n";
					cout << "__________________________________________________________________________________\n\n";
				}
			}
			else if (buy == 4) {
				if (you->getMoney() >= excalibur->getCost()) {
					you->deleteWeapon();
					you->setWeapon(excalibur);
					you->subtractMoney(excalibur->getCost());
					cout << "You got and wore the " << excalibur->getName() << ".\n";
				}
				else if (you->getMoney() < excalibur->getCost()) {
					cout << "__________________________________________________________________________________\n";
					cout << "SHOPKEEPER: You don't have enough.\n";
					cout << "__________________________________________________________________________________\n\n";
				}
			}
			else {
				cout << "What else interests you?\n";
			}
			system("pause");
			system("cls");
			break;
		case 2:
			cout << "__________________________________________________________________________________\n";
			cout << "SHOPKEEPER: These armors will increase your defense more. (pick a number)\n";
			cout << "__________________________________________________________________________________\n";
			cout << "1. Leather Mail - 2DEF+ (50G)\n" <<
				"2. Chain Mail - 4DEF+ (100G)\n" <<
				"3. Plate Armor - 8DEF+ (300G)\n" <<
				"4. Back\n";
			cin >> buy;

			if (buy == 1) {
				if (you->getMoney() >= lMail->getCost()) {
					you->deleteArmor();
					you->setArmor(lMail);
					you->subtractMoney(lMail->getCost());
					cout << "You got and wore the " << lMail->getName() << ".\n";
				}
				else if (you->getMoney() < lMail->getCost()) {
					cout << "__________________________________________________________________________________\n";
					cout << "SHOPKEEPER: You don't have enough.\n";
					cout << "__________________________________________________________________________________\n\n";
				}
			}
			else if (buy == 2) {
				if (you->getMoney() >= cMail->getCost()) {
					you->deleteArmor();
					you->setArmor(cMail);
					you->subtractMoney(cMail->getCost());
					cout << "You got and wore the " << cMail->getName() << ".\n";
				}
				else if (you->getMoney() < cMail->getCost()) {
					cout << "__________________________________________________________________________________\n";
					cout << "SHOPKEEPER: You don't have enough.\n";
					cout << "__________________________________________________________________________________\n\n";
				}
			}
			else if (buy == 3) {
				if (you->getMoney() >= pArmor->getCost()) {
					you->deleteArmor();
					you->setArmor(pArmor);
					you->subtractMoney(pArmor->getCost());
					cout << "You got and wore the " << pArmor->getName() << ".\n";
				}
				else if (you->getMoney() < pArmor->getCost()) {
					cout << "__________________________________________________________________________________\n";
					cout << "SHOPKEEPER: You don't have enough.\n";
					cout << "__________________________________________________________________________________\n\n";
				}
			}
			else {
				cout << "__________________________________________________________________________________\n";
				cout << "SHOPKEEPER: Take a look around some more.\n";
				cout << "__________________________________________________________________________________\n\n";
			}
			system("pause");
			system("cls");
			break;
		case 3:
			cout << "__________________________________________________________________________________\n";
			cout << "SHOPKEEPER: Come again.\n";
			cout << "__________________________________________________________________________________\n\n";
			system("pause");
			system("cls");
			shopping = 0; 
			break;
		}
	}
}

void playerMenu(Player* you) {
	int answer, round;
	bool gameOn = 1; 
	Unit* enemy = 0;

	while (you->getHP() > 0 && gameOn == 1) {
		cout << "__________________________________________________________________________________\n";
		cout << "PLAYER MENU\n";
		cout << "(Pick a number)\n";
		cout << "__________________________________________________________________________________\n";
		cout << "1. Explore\n2. View Player Information\n3. Recover\n4. Quit Game\n";
		cin >> answer;

		switch (answer) {
		case 1:
			you->explore();
			if (you->getX() == 1 && you->getY() == 1) {
				shop(you);
			}
			else {
				if (chance() == 1) {
					round = 1;
					cout << "A monster rushes out to attack you!\n";
					enemy = hellSpawn(enemy, you);
					system("pause");
					system("cls");
					fight(you, enemy);
				}
			}
			system("pause");
			system("cls");
			break;
		case 2:
			you->showStats();
			system("pause");
			system("cls");
			break;
		case 3:
			you->recover();
			system("pause");
			system("cls");
			break;
		case 4:
			cout << "You quit the game.\n";
			gameOn = 0; 
			system("pause");
			system("cls");
			break;
		}
	}
}

int main() {
	srand(time(0));
	string name;
	Player* you = 0;

	cout << "Welcome to your new adventure!\n";
	cout << "What's the name of your character?\n";
	cin >> name;
	cout << "\nYou've named your character " << name << ". What kind of class is your character?\n\n";
	you = pickClass(you, name);
	system("pause");
	system("cls");

	you->showStats(); 
	system("pause");
	system("cls");

	playerMenu(you);
}