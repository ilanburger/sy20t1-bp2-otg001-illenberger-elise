#pragma once
#include <string>
#include <iostream>
#include "Unit.h"
using namespace std;

class Item
{
public:
	Item(string name,int value);
	
	string getName();
	int getCost();  

	virtual int getProperties() = 0;

private:
	string mName; 
	int mCost;
};

