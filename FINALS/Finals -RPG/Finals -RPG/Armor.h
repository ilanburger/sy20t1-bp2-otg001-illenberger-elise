#pragma once
#include <string>
#include <iostream>
#include "Item.h"
using namespace std;

class Armor : public Item
{
public:
	Armor(string name, int value);

	int getProperties() override;
private:
	int mDef;
};

