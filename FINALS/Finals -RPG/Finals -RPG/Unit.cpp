#include "Unit.h"
#include "Item.h"


Unit::Unit(string name, string job, int hp, int power, int vitality, int dexterity, int agility)
{
	mName = name;
	mType = job; 
	mHP = hp;
	mMaxHP = mHP;
	mPower = power;
	mVitality = vitality;
	mAgility = agility;
	mDexterity = dexterity;
}

Unit::Unit(string name, string kind, int hp, int exp, int money)
{
	mName = name; 
	mType = kind;
	mHP = hp;
	mMaxHP = mHP;
	mExp = exp;
	mMoney = money; 
}

bool Unit::hitRate(Unit* target)
{
	float hit = (this->mDexterity / target->mAgility) * 100;
	// cap it off by 80 & 20
	if (hit > 80) {
		hit = 80;
	}
	if (hit < 20) {
		hit = 20;
	}
	return rand() % 100 < hit;
}

void Unit::attack(Unit* target)
{
	int targetHP = target->getHP();
	int dmg = (mPower + mDmg) - (target->mVitality + target->getDef());
	if (dmg <= 0) {
		dmg = 1; //cap off at 1.
	}
	cout << dmg << " dmg!\n";
	targetHP -= dmg;
	target->setHP(targetHP);

	cout << mName << " swiped at " << target->mName << "! " << dmg << "DMG total.\n";
	cout << target->mName << " HP: " << target->getHP() << "/" << target->getMaxHP() <<".\n\n";
}

string Unit::getName()
{
	return mName;
}

string Unit::getType()
{
	return mType;
}

int Unit::getHP()
{
	return mHP;
}

int Unit::getMaxHP()
{
	return mMaxHP;
}

void Unit::setMaxHP(int value)
{
	mMaxHP = value;
}

int Unit::getPower()
{
	return mPower;
}

int Unit::getVitality()
{
	return mVitality;
}

int Unit::getAgility()
{
	return mAgility;
}

int Unit::getDexterity()
{
	return mDexterity;
}

void Unit::setHP(int value)
{
	mHP = value;
	if (mHP > getMaxHP()) {
		mHP = getMaxHP();
	}
	if (mHP < 0) {
		mHP = 0;
	}
}

void Unit::setPower(int value)
{
	mPower += value;
}

void Unit::setVitality(int value)
{
	mVitality += value;
}

void Unit::setAgility(int value)
{
	mAgility += value;
}

void Unit::setDexterity(int value)
{
	mDexterity += value;
}

int Unit::getMoney()
{
	return mMoney;
}

void Unit::addMoney(int price)
{
	mMoney += price;
}

void Unit::subtractMoney(int price)
{
	mMoney -= price;
}

int Unit::getExp()
{
	return mExp;
}

void Unit::setExp(int exp)
{
	mExp += exp; 
}

void Unit::setArmor(Item* armor)
{
	mArmor = armor;
	mDef = armor->getProperties();
}

void Unit::deleteArmor()
{
	delete mArmor;
	mArmor = 0;
}

string Unit::getArmorName()
{
	return mArmor->getName();
}

int Unit::getDef()
{
	return mDef; 
}

void Unit::setWeapon(Item* weapon)
{
	mWeapon = weapon;
	mDmg = weapon->getProperties(); 
}

void Unit::deleteWeapon()
{
	delete mWeapon;
	mWeapon = 0; 
}

string Unit::getWeaponName()
{
	return mWeapon->getName();
}

int Unit::getDmg()
{
	return mDmg;
}



