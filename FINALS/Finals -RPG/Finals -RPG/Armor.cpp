#include "Armor.h"
Armor::Armor(string name, int value)
	: Item(name, value)
{
	if (name == "Leather Mail") { mDef = 2; }
	else if (name == "Chain Mail") { mDef = 4; }
	else if (name == "Plate Armor") { mDef = 8; }
	else { mDef = 1; }
}

int Armor::getProperties()
{
	return mDef;
}
