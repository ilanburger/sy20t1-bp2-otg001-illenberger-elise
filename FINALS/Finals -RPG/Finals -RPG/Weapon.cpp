#include "Weapon.h"

Weapon::Weapon(string name, int value)
	: Item(name, value)
{
	if (name == "Short Sword") { mDmg = 5; }
	else if (name == "Long Sword") { mDmg = 10; }
	else if (name == "Broad Sword") { mDmg = 20; }
	else if (name == "Excalibur Sword") { mDmg = 999; }
	else if (name == "Dull Claymore") { mDmg = 3; }
	else { mDmg = 1;  }
}

int Weapon::getProperties()
{
	return mDmg;
}
